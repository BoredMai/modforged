const Credentials = {
  USER_AGENT: 'Modforged',
  CLIENT_ID: '',
  CLIENT_SECRET: '',
  REFRESH_TOKEN: '',
  SUBREDDIT: '',
  QUEST: '',
  QCR: [],
  SIGNATURE: ''
};

module.exports = Credentials;
