#!/usr/bin/env node

const log4js = require('log4js');
const pkg = require('./package.json');

const Reddit = require('./modules/reddit.js');
const Discord = require('./modules/discord.js');

async function setup() {
  log4js.configure({
    appenders: {
      out: { type: 'stdout', layout: { type: 'coloured' } },
      reddit: {
        type: 'file',
        filename: 'log/reddit.log',
        layout: {
          type: 'pattern',
          pattern: '[%d{yyyy-MM-dd hh:mm:ss}] [%p] [Reddit] %m'
        }
      },
      discord: {
        type: 'file',
        filename: 'log/discord.log',
        layout: {
          type: 'pattern',
          pattern: '[%d{yyyy-MM-dd hh:mm:ss}] [%p] [Discord] %m'
        }
      }
    },
    categories: {
      default: { appenders: ['out'], level: 'debug' },
      reddit: { appenders: ['out', 'reddit'], level: 'debug' },
      discord: { appenders: ['out', 'discord'], level: 'debug' }
    }
  });

  const Logger = log4js.getLogger();
  Logger.info(`Starting Modforged ${pkg.version}`);

  try {
    await Reddit.setup(log4js.getLogger('reddit'));
    await Discord.setup(log4js.getLogger('discord'), pkg.version);
    console.log('[MODFORGED] Startup completed');
  } catch (err) {
    console.log('[INITIALIZATION ERROR]', err);
    process.exit(1);
  }
}

setup();
