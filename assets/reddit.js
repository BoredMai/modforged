const Assets = {
  MESSAGE: {
    NEW_QCR_COMMENT: 'New QCR comment -> $0',
    SIGNATURE:
      '\n\n---\n\n^(This is a Modbot automated response. For error reports, suggestions and the sort, please contact BoredMai through) ^[Reddit](/u/BoredMai) ^(or Discord.)',
    LOG: 'Thread: $0 | Author: $1 | $2'
  },
  ERROR: {
    QCR_FAILED: 'Error adding patron from comment $0\n$1'
  }
};

module.exports = Assets;
