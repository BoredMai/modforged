const ERROR = 'red_x.png';
const INFO = 'info.png';
const SUCCESS = 'green_check.png';
const WARNING = 'warning.png';

module.exports = {
  ERROR,
  INFO,
  SUCCESS,
  WARNING
};
