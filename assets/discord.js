const Assets = {
  COMMANDS: {
    get: {
      adminOnly: false,
      help:
        "`$0 get patronName` - Returns basic QCR info from a character. If there's more than one result, returns a list of characters.",
      numArgs: 1
    },
    intro: {
      adminOnly: true,
      help: '`$0 intro patronName` - Adds a patron to the active QCR list.',
      numArgs: 1
    },
    outro: {
      adminOnly: true,
      help: '`$0 outro patronName` - Removes a patron to the active QCR list.',
      numArgs: 1
    },
    claim: {
      adminOnly: true,
      help:
        '`$0 claim questId threadUrl` - Claims quest with the informed id and thread URL.',
      numArgs: 2
    },
    drop: {
      adminOnly: true,
      help: '`$0 drop questId` - Drops quest with the informed id.',
      numArgs: 1
    },
    wrap: {
      adminOnly: true,
      help:
        '`$0 wrap questId threadUrl` - Wraps quest with the informed id and thread URL.',
      numArgs: 2
    },
    price: {
      adminOnly: true,
      help:
        '`$0 price rarity itemName` - Fetches price based on rarity: Common/C, Uncommon/U, Rare/R, Very Rare/VR. itemName is optional.',
      numArgs: 1
    },
    help: {
      adminOnly: false,
      numArgs: 0
    }
  },
  ERROR: {
    BROADCAST: 'Broadcast error | Type: $0 | Message: $1 | URL: $2',
    COMMAND_NOT_FOUND: '`$0` - Command not found.',
    FORBIDDEN: 'You do not have permission to execute this command.',
    GUILD_ADMIN_ONLY:
      'You cannot use admin commands on direct messages. Please try again in a channel.',
    INTERNAL_ERROR: 'There was an internal error executing this command.',
    MISSING_PARAMS: 'Not enough parameters - see `$0 help $1.`',
    PRICE_INVALID_RARITY: '`$0` is not a valid item rarity.',
    REPORT_ERROR: 'Internal Error'
  },
  MESSAGE: {
    BROADCAST: 'New $0!',
    HELP_HEADER: 'Modforged Commands',
    MULTIPLE_RESULTS_FOUND: '$0 results found:',
    MULTIPLE_RESULTS_FOOTER: '...and $0 more.'
  },
  PREFIX: ['modforged', 'mf', 'mb'],
  PRICE: {
    COMMON: {
      min: 4,
      max: 14,
      multiplier: 5,
      rarityName: 'Common'
    },
    UNCOMMON: {
      min: 2,
      max: 12,
      multiplier: 50,
      rarityName: 'Uncommon'
    },
    RARE: {
      min: 4,
      max: 40,
      multiplier: 500,
      rarityName: 'Rare'
    },
    VERYRARE: {
      min: 40,
      max: 100,
      multiplier: 500,
      rarityName: 'Very Rare'
    }
  }
};

Assets.PRICE.C = Assets.PRICE.COMMON;
Assets.PRICE.U = Assets.PRICE.UNCOMMON;
Assets.PRICE.R = Assets.PRICE.RARE;
Assets.PRICE.VERY = Assets.PRICE.VERYRARE;
Assets.PRICE.VR = Assets.PRICE.VERYRARE;

Assets.MODULES = {
  QCR: [Assets.COMMANDS.get, Assets.COMMANDS.intro, Assets.COMMANDS.outro],
  QUESTS: [Assets.COMMANDS.claim, Assets.COMMANDS.drop, Assets.COMMANDS.wrap]
};

module.exports = Assets;
