const UPDATE_PREFIX = '[$0] $1 - Quest ';

const Assets = {
  ERROR: {
    NO_ID: "You haven't supplied a quest ID.",
    NOT_FOUND: 'Cannot find quest with ID $0.',
    UNAVAILABLE: 'Unable to $0 quest with $1 status.'
  },
  MESSAGE: {
    CLAIM: UPDATE_PREFIX + 'claimed.',
    DROP: UPDATE_PREFIX + 'dropped.',
    WRAP: UPDATE_PREFIX + 'wrapped.'
  }
};

module.exports = Assets;
