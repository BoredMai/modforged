const { Constants, Helpers } = require('./utils.js');
const { ERROR, MESSAGE } = require('../assets/qcr.js');
let Wrap = {};
let Thread = {};
const List = [];

class QCR {
  /* REDDIT */
  setWrap(wrap) {
    Wrap = wrap;
    return this;
  }

  setThreads(idList) {
    let promiseList = [];
    idList.forEach(id => promiseList.push(this.setThread(id)));
    return Promise.all(promiseList);
  }

  async setThread(id) {
    try {
      console.log(`[QCR] Fetching thread ID: ${id}`);
      const thread = await Wrap.getSubmission(id).expandReplies({ depth: 1 });
      const { archived, comments, title } = thread;
      if (!archived) {
        Thread = thread;
      }
      List.push(...comments);

      const prefix = archived ? '[QCR - LEGACY]' : '[QCR - ACTIVE]';
      console.log(`${prefix} Fetched thread ID: ${id} | Title: ${title}`);
    } catch (err) {
      throw err;
    }
  }

  async fromReddit(comment) {
    try {
      const { body, permalink } = comment;
      const name = Helpers.getNameFromQCR(body);
      return this.addPatron(name, permalink, comment);
    } catch (err) {
      throw err;
    }
  }
  /* END REDDIT */

  /* DISCORD */
  fromDiscord(command, name) {
    const handler = `${command}Handler`;
    return this[handler](name);
  }

  async getHandler(name) {
    try {
      const data = await this.findPatronByName(name);
      if (data.length === 0) {
        return {
          error: true,
          message: Helpers.formatMessage(ERROR.NOT_FOUND, name)
        };
      }

      return { data };
    } catch (err) {
      throw err;
    }
  }

  async introHandler(name) {
    try {
      const data = await this.findPatronByName(name);

      if (data.length === 0) {
        return {
          error: true,
          message: Helpers.formatMessage(ERROR.NOT_FOUND, name)
        };
      }

      if (data.length > 1) {
        return { data };
      }

      const { fullName, isActive, url } = data.pop();
      if (isActive) {
        return {
          error: true,
          message: Helpers.formatMessage(ERROR.ACTIVE_PATRON, fullName)
        };
      }

      return this.addPatron(fullName, url);
    } catch (err) {
      throw err;
    }
  }

  async outroHandler(name) {
    try {
      const qcr = await Thread.refresh();

      const data = await this.findPatronByName(name);
      if (data.length === 0) {
        return {
          error: true,
          message: Helpers.formatMessage(ERROR.NOT_FOUND, name)
        };
      }

      if (data.length > 1) {
        return { data };
      }

      const { comment, fullName, isActive } = data.pop();
      if (!isActive) {
        return {
          error: true,
          message: Helpers.formatMessage(ERROR.RETIRED_PATRON, name)
        };
      }

      const regex = new RegExp(`\\n\\n\\* \\[${fullName}].*`);
      const content = qcr.selftext.replace(regex, '');

      qcr.edit(content);

      return {
        message: Helpers.formatMessage(
          MESSAGE.PATRON_REMOVED,
          fullName,
          comment.permalink
        )
      };
    } catch (err) {
      throw err;
    }
  }
  /* END DISCORD */

  /* GENERAL */
  async findPatronByName(name) {
    try {
      const qcr = await Thread.refresh();
      let results = [];
      for (let comment of List) {
        const fullName = Helpers.getNameFromQCR(comment.body);

        if (fullName.toLowerCase().includes(name.toLowerCase())) {
          const isActive = qcr.selftext.includes(comment.id);
          const createdOnDate = new Date(0);
          createdOnDate.setUTCSeconds(comment.created);
          const createdOn = createdOnDate.toLocaleDateString('en-US', {
            year: 'numeric',
            month: 'short',
            day: 'numeric'
          });

          const status = isActive
            ? Constants.QCR_STATUS.ACTIVE
            : Constants.QCR_STATUS.RETIRED;

          const res = results.find(p => p.fullName === fullName);
          if (res) {
            if (createdOnDate < res.createdOn) {
              continue;
            } else {
              results.splice(results.indexOf(res), 1);
            }
          }

          results.push({
            author: comment.author.name,
            comment,
            createdOn,
            fullName,
            isActive,
            status,
            url: Helpers.getUrlFromComment(comment)
          });
        }
      }
      return results;
    } catch (err) {
      throw err;
    }
  }

  async addPatron(newPatron, url, comment) {
    try {
      const qcr = await Thread.refresh();

      const sectionHeader = `**${newPatron[0]}**\n\n`;
      const fromIndex = qcr.selftext.indexOf(sectionHeader) + 7;
      const toIndex = qcr.selftext.indexOf('**', fromIndex);
      const block = qcr.selftext.slice(fromIndex, toIndex);
      const blockArray = block.split('\n\n');

      let listIndex;
      blockArray.some((patron, index) => {
        const patronName = Helpers.removeLink(patron.slice(2));
        if (!patronName || patronName > newPatron) {
          blockArray.splice(index, 0, `* [${newPatron}](${url})`);
          listIndex = index;
          return true;
        }
      });
      const content = qcr.selftext.replace(
        `${sectionHeader}${block}`,
        `${sectionHeader}${blockArray.join('\n\n')}`
      );
      qcr.edit(content);
      if (comment) {
        List.push(comment);
      }
      return {
        message: Helpers.formatMessage(MESSAGE.PATRON_ADDED, newPatron),
        index: listIndex
      };
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new QCR();
