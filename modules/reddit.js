const Snoowrap = require('snoowrap');
const { CommentStream, SubmissionStream } = require('snoostorm');
const QCR = require('./qcr.js');
const Quests = require('./quests.js');
const Discord = require('./discord.js');
const { Helpers } = require('./utils.js');
const Credentials = require('../credentials/reddit.js');
const { ERROR, MESSAGE } = require('../assets/reddit');
let Logger;

class Reddit {
  log(message) {
    console.log(`[REDDIT] ${message}`);
  }

  async setup(logger) {
    Logger = logger;

    this.log(`Setting up module`);

    try {
      const Wrap = new Snoowrap({
        userAgent: Credentials.USER_AGENT,
        clientId: Credentials.CLIENT_ID,
        clientSecret: Credentials.CLIENT_SECRET,
        refreshToken: Credentials.REFRESH_TOKEN,
      });
      Wrap.config({
        continueAfterRatelimitError: true,
        requestDelay: 1000,
        warnings: true,
      });

      this.log(`Successfully logged in`);

      await Quests.setWrap(Wrap).setThread(Credentials.QUEST);
      await QCR.setWrap(Wrap).setThreads(Credentials.QCR);

      const config = {
        subreddit: Credentials.SUBREDDIT,
        limit: 10,
        pollTime: 2000,
      };

      const submissions = new SubmissionStream(Wrap, config);
      submissions.on('item', this.onNewSubmission.bind(this));
      const comments = new CommentStream(Wrap, config);
      comments.on('item', this.onNewComment.bind(this));
    } catch (err) {
      Logger.error(err);
    }
  }

  extractTag(title) {
    const [tag] = title.match(/[\[\(][a-z\- ]*[\]\)]/gi) || [];
    return tag ? tag.toUpperCase().replace(/[\(\)\[\] -]/g, '') : 'THREAD';
  }

  async onNewComment(comment) {
    const {
      author,
      created_utc,
      id,
      link_id,
      link_permalink,
      parent_id,
    } = comment;
    if (Helpers.isThreadOld(created_utc)) return;

    const threadId = parent_id.slice(3);
    if (Credentials.QCR.includes(threadId)) {
      if (parent_id != link_id) {
        Logger.info(
          Helpers.formatMessage(MESSAGE.NEW_QCR_COMMENT, link_permalink + id),
        );
        return;
      }

      try {
        const { index, message } = await QCR.fromReddit(comment);
        Logger.info(`${message} | Index: ${index}`);
      } catch (err) {
        const message = Helpers.formatMessage(
          ERROR.QCR_FAILED,
          link_permalink,
          err.message,
        );
        Discord.report(link_permalink, author, message);
        Logger.error(message);
        return this.sendSignedResponse(comment, message, true);
      }
    }
  }

  async onNewSubmission(thread) {
    const { author, created_utc, title, url } = thread;
    if (Helpers.isThreadOld(created_utc)) return;

    const tag = this.extractTag(title);

    try {
      if (tag === 'QUEST' || tag === 'WRAPUP') {
        const { error, message } = await Quests.fromReddit(thread, tag);
        const logMessage = Helpers.formatMessage(
          MESSAGE.LOG,
          url,
          author.name,
          message,
        );

        if (error) {
          Logger.error(logMessage);
        } else {
          Logger.info(logMessage);
          this.sendSignedResponse(thread, message, error);
        }
      }

      return Discord.broadcast(thread, tag);
    } catch (err) {
      Logger.error(
        Helpers.formatMessage(MESSAGE.LOG, url, author.name, err.message),
      );
      Discord.report(url, author.name, err.message);
    }
  }

  sendSignedResponse(thread, message, error = false) {
    const response = error ? Helpers.addErrorPrefix(message) : message;
    thread.reply(response + MESSAGE.SIGNATURE);
  }
}

module.exports = new Reddit();
