# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.4.4](https://gitlab.com/BoredMai/modforged/compare/v3.4.0...v3.4.4) (2019-11-26)


### Bug Fixes

* **discord:** accept lower case prefix ([bb1c8e8](https://gitlab.com/BoredMai/modforged/commit/bb1c8e8))
* **qcr:** push new QCR comment to list - fixes [#20](https://gitlab.com/BoredMai/modforged/issues/20) ([1642621](https://gitlab.com/BoredMai/modforged/commit/1642621))
* **reddit:** remove no-id error message ([ac6cf29](https://gitlab.com/BoredMai/modforged/commit/ac6cf29))
* **utils:** trim QCR line before cleaning name - fixes [#19](https://gitlab.com/BoredMai/modforged/issues/19) ([fd6614d](https://gitlab.com/BoredMai/modforged/commit/fd6614d))

### [3.4.3](https://gitlab.com/BoredMai/modforged/compare/v3.4.0...v3.4.3) (2019-10-17)


### Bug Fixes

* **discord:** accept lower case prefix ([bb1c8e8](https://gitlab.com/BoredMai/modforged/commit/bb1c8e8))
* **qcr:** push new QCR comment to list - fixes [#20](https://gitlab.com/BoredMai/modforged/issues/20) ([1642621](https://gitlab.com/BoredMai/modforged/commit/1642621))
* **utils:** trim QCR line before cleaning name - fixes [#19](https://gitlab.com/BoredMai/modforged/issues/19) ([fd6614d](https://gitlab.com/BoredMai/modforged/commit/fd6614d))

### [3.4.2](https://gitlab.com/BoredMai/modforged/compare/v3.4.0...v3.4.2) (2019-09-30)


### Bug Fixes

* **discord:** accept lower case prefix ([bb1c8e8](https://gitlab.com/BoredMai/modforged/commit/bb1c8e8))

## [3.4.0](https://gitlab.com/BoredMai/modforged/compare/v3.3.0...v3.4.0) (2019-09-29)


### Bug Fixes

* **discord:** add /u/ to user link on QCR block ([390d6ff](https://gitlab.com/BoredMai/modforged/commit/390d6ff))
* **discord:** support multiple aliases, update matching regex ([5598dc1](https://gitlab.com/BoredMai/modforged/commit/5598dc1)), closes [#16](https://gitlab.com/BoredMai/modforged/issues/16)
* **discord:** use title+url to link QCRs | closes [#17](https://gitlab.com/BoredMai/modforged/issues/17) ([297f6c1](https://gitlab.com/BoredMai/modforged/commit/297f6c1))


### Features

* **app:** add changelog versioning ([0577562](https://gitlab.com/BoredMai/modforged/commit/0577562))
* **discord:** add logging to price command ([03dbdeb](https://gitlab.com/BoredMai/modforged/commit/03dbdeb))
* **discord:** add price command | closes [#9](https://gitlab.com/BoredMai/modforged/issues/9) ([e50e3d3](https://gitlab.com/BoredMai/modforged/commit/e50e3d3))
